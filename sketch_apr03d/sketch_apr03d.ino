// WiFi GET Request Arduino
// by Carlos H. Montenegro
// Created May 8, 2017

#include <WiFiEsp.h>
#include <ArduinoJson.h>

#ifndef HAVE_HWSERIAL1
  #include <SoftwareSerial.h>
  SoftwareSerial WifiModuleSerial(2, 3); // RX, TX
#endif

char ssid[] = "Tekton-MobileROR";        // your network SSID (name)
char pass[] = "c9aa28ba94";        // your network password

int status = WL_IDLE_STATUS;       // the Wifi radio's status

// Request Information

char server[] = "netflixroulette.net";
char apiKey[] = "ba340759a48fe78ba7db6e7c7f2152ea";
char q[]      = "London";

// Response Variables

char json[] = {};
int  index = 0;

WiFiEspClient client;

void setup() {
  // initialize serial for debugging
  Serial.begin(9600);
  // initialize serial for ESP module
  WifiModuleSerial.begin(9600);
  // initialize ESP module
  WiFi.init(&WifiModuleSerial);

  // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println(F("WiFi shield not present"));
    // don't continue
    while (true);
  }

  // attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    Serial.print(F("Attempting to connect to WPA SSID: "));
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
  }

  // you're connected now, so print out the data
  Serial.println(F("You're connected to the network"));
  
  printWifiStatus();

  Serial.println();
  Serial.println(F("Starting connection to server..."));
  // if you get a connection, report back via serial
  if (client.connect(server, 80)) {
    Serial.println(F("Connected to server"));
    // Make a HTTP request
    client.println(F("GET /api/api.php?title=Attack%20on%20titan HTTP/1.1"));
    client.println(F("Host: netflixroulette.net"));
    client.println(F("Connection: close"));
    client.println();
  }
}

void loop() {
  while (client.available()) {
    char c = client.read();
    //json[index] = c;
    //index++;
    Serial.write(c);
  }
  //json[index] = '\0';

  //Serial.println(json);

  if (!client.connected()) {
    Serial.println();
    Serial.println(F("Disconnecting from server..."));
    client.stop();
    while (true);
  }
}

void printWifiStatus() {
  // print the SSID of the network you're attached to
  Serial.print(F("SSID: "));
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print(F("IP Address: "));
  Serial.println(ip);

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print(F("Signal strength (RSSI):"));
  Serial.print(rssi);
  Serial.println(F(" dBm"));
}

