#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C  lcd(0x3F,2,1,0,4,5,6,7); // 0x3F is the I2C bus address for the current backpack

class LCDWrapper {
  
  private: int cursor [];
  private: int xColumns, yRows;
  
  // Constructors
  public: LCDWrapper();
  public: LCDWrapper(int, int);

  // Methods
  public: moveCursorUp();
  public: moveCursorDown();
  public: moveCursorRight();
  public: moveCursorLeft();
  public: returnCursor();
  public: writeCharacter(char);
  public: deleteCharacter();
  
};

LCDWrapper:: LCDWrapper() {
  xColumns     = 16;
  yRows        = 2;
  
  for ( int i = 0; i < yRows; i++ ) { cursor[i] = 0; }
  
}
LCDWrapper:: LCDWrapper(int x, int y) {
  xColumns     = x;
  yRows        = y;
  
  for ( int i = 0; i < yRows; i++ ) { cursor[i] = 0; }
}

LCDWrapper:: moveCursorUp() {
  if (cursor[1] != 0) { cursor[1]--; }
  lcd.setCursor(cursor[0], cursor[1]);
}
LCDWrapper:: moveCursorDown() {
  if (cursor[1] != yRows - 1) { cursor[1]++; }
  lcd.setCursor(cursor[0], cursor[1]);
}
LCDWrapper:: moveCursorLeft() {
  if (cursor[0] != 0) { cursor[0]--; }
  lcd.setCursor(cursor[0], cursor[1]);
}
LCDWrapper:: moveCursorRight() {
  if (cursor[0] != xColumns - 1) { cursor[0]++; }
  lcd.setCursor(cursor[0], cursor[1]);
}
LCDWrapper:: returnCursor() {
  cursor[0] = 0;
  cursor[1]++;
  lcd.setCursor(cursor[0], cursor[1]);
}
LCDWrapper:: writeCharacter(char character) {
  if (cursor[0] != xColumns - 1) {
    lcd.setCursor(cursor[0], cursor[1]);
    lcd.write(character);
  } else {
    cursor[0] = 0;
    cursor[1]++;
    lcd.setCursor(cursor[0], cursor[1]);
  }
}
LCDWrapper:: deleteCharacter() {
  lcd.setCursor(cursor[0], cursor[1]);
  lcd.write((char) 32);
}

int character;
LCDWrapper wrapper;

void setup() {
  
  //Setup LCD Module
  wrapper = LCDWrapper(16, 2);

  lcd.begin(16, 2);
  lcd.cursor();
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(1);
    
  //Setup Serial for Bluetooth
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() {
    
  if(Serial.available() > 0) {
    character = Serial.read();
    switch(character) {
      case 127: // Delete
        wrapper.deleteCharacter();
        wrapper.moveCursorLeft();
        break;
      case 13:  // Enter
        wrapper.returnCursor();
        break;
      case 'A': // UP Arrow
        wrapper.moveCursorUp();
        break;
      case 'B': // DOWN Arrow
        wrapper.moveCursorDown();
        break;
      case 'C': // RIGHT Arrow
        wrapper.moveCursorRight();
        break;
      case 'D':  // LEFT Arrow
        wrapper.moveCursorLeft();
        break;
      default:
        wrapper.writeCharacter(character);
        wrapper.moveCursorRight();
        break;
    }
  }
}

